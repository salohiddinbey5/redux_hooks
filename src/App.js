import React, { useState } from "react"
import { connect,useSelector,useDispatch } from "react-redux";
import { add_todo } from './action'
import { delete_todo } from "./action";
import { clear_todo } from "./action";
import { edit_todo } from './action'


const App = ({ dispatch, globalState }) => {
  // console.log(props);

  const [value, setValue] = React.useState("");
  // const state= useSelector(state=>state)
  // const dispatch =useDispatch()


  const handleChange = (e) => {
    e.preventDefault();
    // console.log(e.target.value);
    setValue(e?.target?.value)
  }

  const onSubmit = () => {
    dispatch(add_todo(value))
    // const update = [...todos,value]
    // setTodos(update)
    // console.log(update);
    setValue('')
  }

  const onDelete = (id) => {
    dispatch(delete_todo(id))
  }

  const onClear = (item) => {
    dispatch(clear_todo(item))
  }

  const onEdit = (item) => {
    console.log(dispatch(edit_todo(item)))
    dispatch(delete_todo(item.id))
    setValue(item.value);
  }

  return (
    <div className="container container-fluid mt-5">
      <input value={value} onChange={handleChange} placeholder="Please enter To do" />
      <button className="btn-primary" onClick={onSubmit}>Add too</button>
      <ul>
        {globalState?.map((item, id) => {
          return (
            <>
              <li className="my-1 mx-1"
                key={id}
              >{item?.value}
                <button className="btn btn-danger" onClick={() => onDelete(item.id)}>
                  Delete
                </button>
                <button className="btn btn-secondary m-1" onClick={() => onEdit(item)}>
                  Edit
                </button></li>
            </>
          )
        })} <br />
        <button className="btn btn-success" onClick={() => onClear()}>Clear All</button>
      </ul>


    </div>
  )
}


const mapStateToProps = (state) => {
  return {
    globalState: state
  }
}


export default connect(mapStateToProps)(App)