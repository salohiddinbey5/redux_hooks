import React from 'react'
import {connect} from 'react-redux'
import { counterInc } from './action'

const Test2 = (props)=>{
	const onUpdateHandler = () =>{
		props.salom(props.count+1)
	}
	return (
		<div style={{borderWidth:3,borderColor:'red'}}>
			<h3>{props.count}</h3>
			<button type='button' onClick={onUpdateHandler}>Bos</button>
		</div>
	)
}

const mapStateToProps =(state)=>{
	return{
		count:state.count
	}
}

const mapDispatchToProps = (state)=>{
	return{
		salom:count =>{
			counterInc(count)
		}
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Test2)
