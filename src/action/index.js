// export const counterInc = (count) => {
//     return {
//         type: "INC",
//         count
//     }
// }

// export const counterDec = (count) =>{
//     return{
//         type:'DECR',
//         count
//     }
// }

import {v4} from "uuid";

export const add_todo = (value) => {
     return {
         type: "ADD_TODO",
         id: v4(),
         value
     }
}

export const delete_todo = (id)=>{
return{
    type: "DELETE_TODO",
    id
}
}

export const clear_todo = () =>{
    return {
        type: "CLEAR_ALL"
    }
}

export const edit_todo = (item) =>{
    return{
        type: "EDIT_TODO",
        item
    }
}